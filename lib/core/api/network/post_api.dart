import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:prueba_ceiba/core/api/repository/post_repository.dart';
import 'package:prueba_ceiba/core/exception/post_exception.dart';
import 'package:prueba_ceiba/core/model/post_model.dart';

class PostApi implements PostRepository {
  final Dio dio;
  final _urlBase = dotenv.env['URL_BASE'];

  PostApi({required this.dio});

  @override
  Future<List<PostModel>> getPostbByUserId(int id) async {
    var listOfPost = <PostModel>[];
    try {
      var result = await dio.get('$_urlBase/posts?userId=$id');
      if (result.data != null) {
        for (var item in result.data) {
          listOfPost.add(PostModel.fromJson(item));
        }
      }
    } on Exception catch (e) {
      throw PostException(
          message: 'Error al obtener los datos del usuario: $e');
    }
    return listOfPost;
  }
}
