import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:prueba_ceiba/core/api/repository/user_repository.dart';
import 'package:prueba_ceiba/core/exception/user_exception.dart';
import 'package:prueba_ceiba/core/model/user_model.dart';

class UserApi implements UserRepository {
  final Dio dio;

  UserApi({required this.dio});
  final _urlBase = dotenv.env['URL_BASE'];
  @override
  Future<List<UserModel>> getAllUsers() async {
    var listOfUser = <UserModel>[];
    try {
      var result = await dio.get('$_urlBase/users');
      if (result.data != null) {
        for (var item in result.data) {
          listOfUser.add(UserModel.fromJson(item));
        }
      }
    } on Exception catch (e) {
      throw UserException(
          message: 'Error al obtener los datos de usuarios: $e');
    }
    return listOfUser;
  }
}
