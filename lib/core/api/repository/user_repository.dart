import 'package:prueba_ceiba/core/model/user_model.dart';

abstract class UserRepository {
  Future<List<UserModel>> getAllUsers();
}
