import 'package:prueba_ceiba/core/model/post_model.dart';

abstract class PostRepository {
  Future<List<PostModel>> getPostbByUserId(int id);
}
