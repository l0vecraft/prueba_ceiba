class PostException implements Exception {
  final String message;

  PostException({required this.message});
}
