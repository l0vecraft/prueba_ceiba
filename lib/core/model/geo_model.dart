import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'geo_model.g.dart';

@JsonSerializable()
@HiveType(typeId: 2)
class GeoModel extends HiveObject {
  @JsonKey(name: 'lat')
  @HiveField(0)
  String? lat;
  @JsonKey(name: 'lng')
  @HiveField(1)
  String? lng;

  GeoModel({this.lat, this.lng});

  factory GeoModel.fromJson(Map<String, dynamic> json) =>
      _$GeoModelFromJson(json);

  Map<String, dynamic> toJson() => _$GeoModelToJson(this);
}
