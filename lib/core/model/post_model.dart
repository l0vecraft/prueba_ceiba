import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'post_model.g.dart';

@JsonSerializable()
@HiveType(typeId: 3)
class PostModel extends HiveObject {
  @JsonKey(name: 'userId')
  @HiveField(0)
  final int? userId;
  @JsonKey(name: 'id')
  @HiveField(1)
  final int? id;
  @JsonKey(name: 'title')
  @HiveField(2)
  final String? title;
  @JsonKey(name: 'body')
  @HiveField(3)
  final String? body;

  PostModel({this.userId, this.id, this.title, this.body});

  factory PostModel.fromJson(Map<String, dynamic> json) =>
      _$PostModelFromJson(json);

  Map<String, dynamic> toJson() => _$PostModelToJson(this);
}
