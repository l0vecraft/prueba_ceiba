import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:prueba_ceiba/core/model/geo_model.dart';

part 'address_model.g.dart';

@JsonSerializable()
@HiveType(typeId: 0)
class AddressModel extends HiveObject {
  @JsonKey(name: 'street')
  @HiveField(0)
  String? street;
  @JsonKey(name: 'suite')
  @HiveField(1)
  String? suite;
  @JsonKey(name: 'city')
  @HiveField(2)
  String? city;
  @JsonKey(name: 'zipcode')
  @HiveField(3)
  String? zipcode;
  @JsonKey(name: 'geo')
  @HiveField(4)
  GeoModel? geo;

  AddressModel({this.street, this.suite, this.city, this.zipcode, this.geo});

  factory AddressModel.fromJson(Map<String, dynamic> json) =>
      _$AddressModelFromJson(json);

  Map<String, dynamic> toJson() => _$AddressModelToJson(this);
}
