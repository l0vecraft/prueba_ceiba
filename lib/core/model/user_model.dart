import 'package:hive/hive.dart';
import 'package:prueba_ceiba/core/model/address_model.dart';
import 'package:prueba_ceiba/core/model/company_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
@HiveType(typeId: 4)
class UserModel extends HiveObject {
  @JsonKey(name: 'id')
  @HiveField(0)
  int? id;
  @JsonKey(name: 'name')
  @HiveField(1)
  String? name;
  @JsonKey(name: 'username')
  @HiveField(2)
  String? username;
  @JsonKey(name: 'email')
  @HiveField(3)
  String? email;
  @JsonKey(name: 'address')
  @HiveField(4)
  AddressModel? address;
  @JsonKey(name: 'phone')
  @HiveField(5)
  String? phone;
  @JsonKey(name: 'website')
  @HiveField(6)
  String? website;
  @JsonKey(name: 'company')
  @HiveField(7)
  CompanyModel? company;

  UserModel(
      {this.id,
      this.name,
      this.username,
      this.email,
      this.address,
      this.phone,
      this.website,
      this.company});

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
