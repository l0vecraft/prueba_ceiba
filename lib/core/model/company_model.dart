import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'company_model.g.dart';

@JsonSerializable()
@HiveType(typeId: 1)
class CompanyModel extends HiveObject {
  @JsonKey(name: 'name')
  @HiveField(0)
  String? name;
  @JsonKey(name: 'catchPhrase')
  @HiveField(1)
  String? catchPhrase;
  @JsonKey(name: 'bs')
  @HiveField(2)
  String? bs;

  CompanyModel({this.name, this.catchPhrase, this.bs});

  factory CompanyModel.fromJson(Map<String, dynamic> json) =>
      _$CompanyModelFromJson(json);

  Map<String, dynamic> toJson() => _$CompanyModelToJson(this);
}
