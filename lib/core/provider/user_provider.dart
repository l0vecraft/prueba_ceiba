import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:prueba_ceiba/boxes.dart';
import 'package:prueba_ceiba/core/api/repository/user_repository.dart';
import 'package:prueba_ceiba/core/exception/user_exception.dart';
import 'package:prueba_ceiba/core/model/user_model.dart';
import 'package:prueba_ceiba/locator.dart';

class UserProvider extends ChangeNotifier {
  final _api = l.get<UserRepository>();
  var _search = '';
  UserModel? _userSelected;
  List<UserModel> _listOfUsers = [];

  String get search => _search;
  UserModel get userSelected => _userSelected!;
  List<UserModel> get listOfUsers => _listOfUsers;

  set listOfUsers(List<UserModel> value) {
    _listOfUsers = value;
    notifyListeners();
  }

  set userSelected(UserModel value) => _userSelected = value;

  set search(String value) {
    _search = value;
    notifyListeners();
  }

  Future<List<UserModel>> getAllUser() async {
    try {
      final box = Boxes.getUsers();
      var result = box.get('listOfUsers')?.cast<UserModel>();
      if (result != null) {
        _listOfUsers = result;
        log('local storage');
      } else {
        _listOfUsers = await _api.getAllUsers();
        box.put('listOfUsers', _listOfUsers);

        log('request');
      }
      return _listOfUsers;
    } on UserException catch (_) {
      rethrow;
    }
  }
}

final userProvider =
    ChangeNotifierProvider<UserProvider>((ref) => UserProvider());

final userFutureData = FutureProvider<List<UserModel>>((ref) async {
  var users = ref.watch(userProvider);
  return users.search.isEmpty
      ? users.getAllUser()
      : Future.value(users.listOfUsers
          .where(
              (element) => element.name!.toLowerCase().startsWith(users.search))
          .toList());
});
