import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:prueba_ceiba/core/api/repository/post_repository.dart';
import 'package:prueba_ceiba/core/exception/post_exception.dart';
import 'package:prueba_ceiba/core/model/post_model.dart';
import 'package:prueba_ceiba/core/provider/user_provider.dart';
import 'package:prueba_ceiba/locator.dart';

class PostProvider extends ChangeNotifier {
  final _api = l.get<PostRepository>();
  var _listOfPost = <PostModel>[];

  List<PostModel> get listOfPosts => _listOfPost;
  Future<List<PostModel>> getPostByUser(int id) async {
    try {
      _listOfPost = await _api.getPostbByUserId(id);
      return _listOfPost;
    } on PostException catch (_) {
      rethrow;
    }
  }
}

final postProvider =
    ChangeNotifierProvider<PostProvider>((ref) => PostProvider());

final postFuture = FutureProvider((ref) {
  final user = ref.watch(userProvider).userSelected;
  return ref.watch(postProvider).getPostByUser(user.id!);
});
