import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:prueba_ceiba/core/model/user_model.dart';

class Boxes {
  static Box<List> getUsers() => Hive.box('users');
}
