import 'package:flutter/material.dart';
import 'package:prueba_ceiba/ui/style/app_colors.dart';

class CustomProgressIndicator extends StatelessWidget {
  const CustomProgressIndicator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Center(
        child: CircularProgressIndicator(
          color: AppColors.mainColor,
        ),
      );
}
