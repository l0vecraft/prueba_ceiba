import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_ceiba/ui/style/app_colors.dart';

class IconWithText extends StatelessWidget {
  final IconData icon;
  final String? title;
  final bool? center;
  const IconWithText({
    Key? key,
    required this.icon,
    required this.title,
    this.center = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment:
          center! ? MainAxisAlignment.center : MainAxisAlignment.start,
      children: [
        Icon(
          icon,
          color: AppColors.mainColor,
        ),
        SizedBox(width: 4.w),
        Text(title!)
      ],
    );
  }
}
