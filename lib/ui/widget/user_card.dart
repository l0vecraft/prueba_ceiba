import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_ceiba/core/model/user_model.dart';
import 'package:prueba_ceiba/ui/style/app_colors.dart';
import 'package:prueba_ceiba/ui/style/app_style.dart';
import 'package:prueba_ceiba/ui/widget/icon_with_text.dart';

class UserCard extends StatelessWidget {
  final UserModel user;
  final VoidCallback onPress;
  const UserCard({
    Key? key,
    required this.user,
    required this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 15.h),
      child: Container(
        height: 170.h,
        child: Card(
          elevation: 6,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 10.h),
                  child: Text(user.name!, style: AppStyle.cardTitle1),
                ),
                IconWithText(
                  icon: Icons.phone,
                  title: user.phone,
                ),
                IconWithText(
                  icon: Icons.email,
                  title: user.email,
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: TextButton(
                      onPressed: onPress,
                      style: ButtonStyle(
                          overlayColor: MaterialStateProperty.resolveWith(
                              (states) =>
                                  AppColors.mainColor.withOpacity(.15))),
                      child: Text('VER PUBLICACIONES',
                          style: TextStyle(color: AppColors.mainColor))),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
