import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_ceiba/core/provider/post_provider.dart';
import 'package:prueba_ceiba/core/provider/user_provider.dart';
import 'package:prueba_ceiba/ui/style/app_colors.dart';
import 'package:prueba_ceiba/ui/style/app_style.dart';
import 'package:prueba_ceiba/ui/widget/custom_progress_indicator.dart';
import 'package:prueba_ceiba/ui/widget/icon_with_text.dart';

class DetailScreen extends ConsumerStatefulWidget {
  const DetailScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _DetailScreenState();
}

class _DetailScreenState extends ConsumerState<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    var user = ref.watch(userProvider).userSelected;
    var listOfPost = ref.watch(postProvider).listOfPosts;
    var postsData = ref.watch(postFuture);
    return Scaffold(
      appBar: AppBar(
        title: Text(user.name!),
        backgroundColor: AppColors.mainColor,
      ),
      body: postsData.when(
          data: (posts) => listOfPost.isEmpty
              ? const Center(
                  child: Text('List is empty.'),
                )
              : Column(
                  children: [
                    Center(
                        child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 8.h),
                      child: CircleAvatar(
                        backgroundColor: AppColors.mainColor,
                        radius: 30.sp,
                        child: Text(
                          user.name![0],
                          style:
                              TextStyle(fontSize: 30.sp, color: Colors.white),
                        ),
                      ),
                    )),
                    Text(user.name!, style: AppStyle.cardTitle1),
                    IconWithText(
                      icon: Icons.phone,
                      center: true,
                      title: user.phone,
                    ),
                    IconWithText(
                      center: true,
                      icon: Icons.email,
                      title: user.email,
                    ),
                    Expanded(
                      child: ListView.separated(
                          itemCount: posts.length,
                          separatorBuilder: (context, index) =>
                              Divider(color: AppColors.mainColor),
                          padding: EdgeInsets.symmetric(
                              vertical: 30.h, horizontal: 12.w),
                          itemBuilder: (context, index) => ListTile(
                                title: Text(
                                  listOfPost[index].title!,
                                  style: AppStyle.cardTitle2,
                                ),
                                subtitle: Text(
                                  listOfPost[index].body!,
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              )),
                    ),
                  ],
                ),
          loading: () => const CustomProgressIndicator(),
          error: (error, s) => Center(
                child: Text(error.toString()),
              )),
    );
  }
}
