import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:prueba_ceiba/core/provider/post_provider.dart';
import 'package:prueba_ceiba/core/provider/user_provider.dart';
import 'package:prueba_ceiba/ui/screen/detail_screen.dart';
import 'package:prueba_ceiba/ui/style/app_colors.dart';
import 'package:prueba_ceiba/ui/widget/custom_progress_indicator.dart';
import 'package:prueba_ceiba/ui/widget/user_card.dart';

class HomeScreen extends ConsumerStatefulWidget {
  const HomeScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends ConsumerState<HomeScreen> {
  final _controller = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    Hive.box('users').close();
  }

  @override
  Widget build(BuildContext context) {
    var userData = ref.watch(userFutureData);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.mainColor,
        title: const Text('Prueba de ingreso'),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8.w),
              child: TextField(
                controller: _controller,
                cursorColor: AppColors.mainColor,
                onSubmitted: (value) {},
                onChanged: (value) => ref.watch(userProvider).search = value,
                decoration: const InputDecoration(labelText: 'Buscar usuario'),
              ),
            ),
            userData.when(
                data: (users) => Expanded(
                    child: users.isEmpty
                        ? const Center(
                            child: Text('List is empty.'),
                          )
                        : ListView.builder(
                            itemCount: users.length,
                            padding: EdgeInsets.symmetric(
                                vertical: 30.h, horizontal: 12.w),
                            itemBuilder: (context, index) => UserCard(
                                  user: users[index],
                                  onPress: () {
                                    ref.read(userProvider).userSelected =
                                        users[index];
                                    ref
                                        .read(postProvider)
                                        .getPostByUser(users[index].id!);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const DetailScreen()));
                                  },
                                ))),
                loading: () => const Expanded(child: CustomProgressIndicator()),
                error: (error, s) => Center(
                      child: Text(error.toString()),
                    ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(onPressed: () async {
        await Hive.deleteFromDisk();
      }),
    );
  }
}
