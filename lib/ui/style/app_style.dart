import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'app_colors.dart';

class AppStyle {
  static TextStyle cardTitle1 = TextStyle(
      fontWeight: FontWeight.bold, fontSize: 18.sp, color: AppColors.mainColor);

  static TextStyle cardTitle2 =
      TextStyle(color: AppColors.mainColor, fontWeight: FontWeight.w500);
}
