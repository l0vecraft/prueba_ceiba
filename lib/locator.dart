import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:prueba_ceiba/core/api/network/post_api.dart';
import 'package:prueba_ceiba/core/api/network/user_api.dart';
import 'package:prueba_ceiba/core/api/repository/post_repository.dart';
import 'package:prueba_ceiba/core/api/repository/user_repository.dart';
import 'package:prueba_ceiba/core/model/address_model.dart';
import 'package:prueba_ceiba/core/model/company_model.dart';
import 'package:prueba_ceiba/core/model/geo_model.dart';
import 'package:prueba_ceiba/core/model/post_model.dart';
import 'package:prueba_ceiba/core/model/user_model.dart';

var l = GetIt.I;
var _dio = Dio();
void setUp() {
  l.registerLazySingleton<UserRepository>(() => UserApi(dio: _dio));
  l.registerLazySingleton<PostRepository>(() => PostApi(dio: _dio));
}

Future<void> initHive() async {
  Hive.registerAdapter(UserModelAdapter());
  Hive.registerAdapter(AddressModelAdapter());
  Hive.registerAdapter(GeoModelAdapter());
  Hive.registerAdapter(PostModelAdapter());
  Hive.registerAdapter(CompanyModelAdapter());
}
